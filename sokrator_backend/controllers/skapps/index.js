/**
 * @sokrator controller
 * @autor Cromlec
 *
 * @description
 * Backend functions
 *
**/

var express = require('express');
var app = module.exports = express();
var googleapis = require('googleapis'),
    readline = require('readline');
	
var moment = require("moment");
var security = require('../../config/security');
var secrets = require('../../config/secrets');

app.use(googleapis);
app.use(readline);

app.use(express.bodyParser());

app.get('/skappsREST',function(req,res){ //Return all apps to frontend   
        
    var fs = require('fs'),
        obj

    fs.readFile('config/skapps.json', handleFile); //Return the json apps  
    function handleFile(err, data) {
        if (err) throw err
        obj = JSON.parse(data);
        res.json(obj);
    }       
});

app.get('/oauthGoogle/:id', function(req,res,done) {
	
	var CLIENT_ID = secrets.google.clientID,
		CLIENT_SECRET = secrets.google.clientSecret,
		REDIRECT_URL = secrets.google.callbackURL,
		SCOPE = 'https://www.googleapis.com/auth/drive'; // Get all the data to connect with drive
	
	var auth = new googleapis.OAuth2Client(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL); // try to oauth
	
	googleapis.discover('drive', 'v2').execute(function(err, client) {
	  var url = auth.generateAuthUrl({ scope: SCOPE });
	  var getAccessToken = function(code) {
		auth.getToken(code, function(err, tokens) {
		  if (err) {
			res.json({"error" : "Imposible to connect"})
		  } else {
            res.json({"url" : url}); // return the url to the backend. It will redirect to the page asking for permissions
          }
		  
		});
	  }
	  
	  
	});
});

app.get("/uploadGoogle",function(req,res){
    
    var userId = req.params.id; // get the param of the user 
    var fileUs = req.params.file; // get the param of the user 
    
	var CLIENT_ID = secrets.google.clientID,
	CLIENT_SECRET = secrets.google.clientSecret,
	REDIRECT_URL = secrets.google.callbackURL,
	SCOPE = 'https://www.googleapis.com/auth/drive.file'; // Get all the data to connect with drive
	
    db
	.User
	.find( { '_id':userId,'userApp.app': 'GoogleDrive' }, { "userApp.$" : "1" }, function (error, user) {
	
	
	var auth = new googleapis.OAuth2Client(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);
	googleapis.discover('drive', 'v2').execute(function(err, client) {
		
		var upload = function() {
		auth.setCredentials({
			access_token: user[0].userApp[0].token
		});
		client.drive.files
		  .insert({ title: fileUs.title, mimeType: 'text/plain' })
		  .withMedia('text/plain', fileUs.title)
		  .withAuthClient(auth).execute(function(err, result) {
                if (err) {
                    res.json({"error" : "-1"})
                } else {
                    res.json({"error" : "0"}); // return the url to the backend. It will redirect to the page asking for permissions
                }
            });
		};
		
	});
});
app.get("/getGDriveFiles/:id",function(req,res){
	
    var userId = req.params.id; // get the param of the user   
    
	var CLIENT_ID = secrets.google.clientID,
	CLIENT_SECRET = secrets.google.clientSecret,
	REDIRECT_URL = secrets.google.callbackURL,
	SCOPE = 'https://www.googleapis.com/auth/drive.file';
	
	db
	.User
	.find( { '_id':userId,'userApp.app': 'GoogleDrive' }, { "userApp.$" : "1" }, function (error, user) {
	
		if (error) return response.json(error);
		
		var auth = new googleapis.OAuth2Client(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);
		
		googleapis.discover('drive', 'v2').execute(function(err, client) {
				
			var listFiles = function() {
				
			auth.setCredentials({
				access_token: user[0].userApp[0].token
			});
                
			client.drive.files // files to google drives
			  .list()
			  .withAuthClient(auth).execute(function(err, result) {
                    if (err) {
                        res.json({"error" : "-1"})
                    } else {
                        res.json(result); // return the data files to the backend.
                    }
				});
            };
			listFiles();	
		});
	});
});
