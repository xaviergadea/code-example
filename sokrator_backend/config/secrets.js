module.exports = {
  db: process.env.MONGODB|| 'mongodb://*******',
    
  sessionSecret: process.env.SESSION_SECRET || 'Your Session Secret goes here',

  localAuth: true,

  mailgun: {
    login: process.env.MAILGUN_LOGIN || 'Mailgun SMTP Username',
    password: process.env.MAILGUN_PASSWORD || 'Mailgun SMTP Password'
  },

  sendgrid: {
    user: process.env.SENDGRID_USER || 'SendGrid Username',
    password: process.env.SENDGRID_PASSWORD || 'SendGrid Password'
  },

  nyt: {
    key: process.env.NYT_KEY || 'New York Times API Key'
  },

  facebookAuth: true,
  facebook: {
    clientID: process.env.FACEBOOK_ID || 'Facebook ID',
    clientSecret: process.env.FACEBOOK_SECRET || 'Facebook Secret',
    callbackURL: '/auth/facebook/callback',
    passReqToCallback: true
  },

  githubAuth: true,
  github: {
    clientID: process.env.GITHUB_ID || 'Github ID',
    clientSecret: process.env.GITHUB_SECRET || 'Github Secret',
    callbackURL: '/auth/github/callback',
    passReqToCallback: true
  },

  twitterAuth: true,
  twitter: {
    consumerKey: process.env.TWITTER_KEY || 'Twitter key',
    consumerSecret: process.env.TWITTER_SECRET  || 'Twitter secret',
    callbackURL: '/auth/twitter/callback',
    passReqToCallback: true
  },

  googleAuth: true,
  google: {
    clientID: process.env.GOOGLE_ID || 'Google ID',
    clientSecret: process.env.GOOGLE_SECRET || 'Goolge secret',
    callbackURL: 'auth/gdocs/oauth2callback',
    passReqToCallback: true
  },

  linkedinAuth: true,
  linkedin: {
    clientID: process.env.LINKEDIN_ID || 'Client ID',
    clientSecret: process.env.LINKEDIN_SECRET || 'Client Secret',
    callbackURL: '/auth/linkedin/callback',
    scope: ['r_fullprofile', 'r_emailaddress', 'r_network'],
    passReqToCallback: true
  },

  tumblr: {
    consumerKey: process.env.TUMBLR_KEY || 'Consumer Key',
    consumerSecret: process.env.TUMBLR_SECRET || 'Consumer Secret',
    callbackURL: '/auth/tumblr/callback'
  }
};
