'use strict';

/**
 * @sokrator controller
 * @autor Cromlec
 *
 * @description
 * Connect apps with backend
 * Add, Use and Delete the app
 *
**/

angular.module('myApp.skappscontrollers', ['angularMoment'])

.controller('skapps', ['$scope','$http','backpath', function($scope,$http,backpath) {
    var url = backpath.url+'skappsREST'; // URL where the Node.js server is running	
    $http.get(url).success(function(data) {
        $scope.url=backpath.url;
        $scope.apps = data;

    })
}])
.controller('skappConnect', ['$scope','$http', '$location','backpath','$window','$cookieStore','$gettextCatalog', function ($scope,$http,$location,backpath,$window,$cookieStore,$gettextCatalog) { 
    $scope.connectapp = function() {
        
		if (confirm(gettextCatalog.getString("connect"))) {
            var url = backpath.url+this.app.routeAuth+'/'+$cookieStore.get("userId"); // URL where the Node.js server is running	
            $http.get(url).success(function(data) {
                if (data.url){
                    $window.location.href = data.url;
				}
            });
        }
    }
	$scope.viewFilesapp = function() {        
		var url = backpath.url+this.app.routeGet+'/'+$cookieStore.get("userId"); // URL where the Node.js server is running	
		$http.get(url).success(function(data) {
			$(data).each(function(index, element) {	//Each file from API response to the scope			
                $scope.files = element.items;
            });
		});         
    }
	$scope.removeApp = function() {        
		if (confirm(gettextCatalog.getString("connect"))) {
            var url = backpath.url+'deleteApp/'+$cookieStore.get("userId")+"/GoogleDrive"; // URL where the Node.js server is running	
            $http.get(url).success(function(data) {
                $(data).each(function(index, element) {	//Each file from API response to the scope			
                    $scope.files = element.items;
                });
            });   
        }
    }
	
 }])
.controller('returnAPI', ['$scope','$http', '$location','backpath','$window','$routeParams','$cookieStore', function ($scope,$http,$location,backpath,$window,$routeParams,$cookieStore) { 
	var url = backpath.url+'userSaveApp/'+$cookieStore.get("userId")+"/GoogleDrive/"+$routeParams.id; // URL where the Node.js server is running	
	
	$http.get(url).success(function(data) {
        $window.location.href = "/app/#/skapps";
	});
 }]);